package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;

import javax.print.attribute.standard.Media;
import javax.swing.*;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        GroupChat groupChat = groupChatDAO.getGroupChat(groupChatId);

        groupChat.setMessageList(groupChatDAO.getGroupChatMessages(groupChatId));
        groupChat.setUserList(groupChatDAO.getGroupChatUsers(groupChatId));

        return groupChat;
    }

    @GET
    @Path("/user/{userId}")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChats(@PathParam("userId") int userId){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        ArrayList<GroupChat> groupChatList = groupChatDAO.getGroupChatByUserId(userId);
        return groupChatList;
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat groupChat){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.addGroupChat(groupChat);
    }

    @GET
    @Path("{groupChatId}/message")
    @Produces(MediaType.APPLICATION_JSON)
    public ArrayList<Message> getAllMessages(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        return groupChatDAO.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("{groupChatId}/message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message message) {
        GroupChatDAO groupChatDAO = new GroupChatDAO();
        groupChatDAO.addMessage(groupChatId, message);
        return message;
    }
}
